'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
    return gulp.src('src/scss/app.scss')
        .pipe(sass())
        .pipe(gulp.dest('./src'));
});

gulp.task('build-sass', function () {
    return gulp.src('src/scss/app.scss')
        .pipe(sass())
        .pipe(gulp.dest('./dist'));
});

gulp.task('watch', gulp.series('sass', function() { 
    gulp.watch('app/scss/**/*.scss', gulp.series('sass'));
}));