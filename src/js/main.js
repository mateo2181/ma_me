import Store from './store.js';
// Set actions, mutations and initial state
const actions = {
    setProfile(context, payload) {
        context.commit('setProfile', payload);
    },
    setTab(context,payload) {
        context.commit('setTab', payload);
    }
};

const mutations = {
    setProfile(state, payload) {
        state.user = Object.assign(state.user, payload);
        return state;
    },
    setTab(state,payload) {
        state.tab = payload;
        return state;
    }
};

const initialState = {
   user: {
       name: 'Jesica Parker',
       city: 'Newport Beach',
       phone: '(949) 325 - 58694',
       website: 'www.seller.com' 
   },
   tab: 'about'
};

const storeInstance = new Store({
    actions,
    mutations,
    initialState
});

export default storeInstance;