import { LitElement, html, css } from "lit-element";
import storeInstance from '../js/main';

class Reviews extends LitElement {

    render() {
        return html`
            <link rel="stylesheet" type="text/css" href="src/app.css">
            <div class="profile-reviews">
                <div class="mb-1"> 
                    <div class="rating">
                        <i class="icon ion-md-star text-lg mr-1" aria-hidden="true"></i>
                        <i class="icon ion-md-star text-lg mr-1" aria-hidden="true"></i>
                        <i class="icon ion-md-star text-lg mr-1" aria-hidden="true"></i>
                        <i class="icon ion-md-star text-lg mr-1" aria-hidden="true"></i>
                        <i class="icon ion-md-star-outline text-lg mr-1" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="text-sm mb-1"> 6 reviews </div>
            </div>
        `
    }
}

customElements.define('profile-reviews', Reviews);