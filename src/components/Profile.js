import { LitElement, html, css } from 'lit-element';
import storeInstance from '../js/main';

class Profile extends LitElement {

    constructor() {
        super();
        this.user = storeInstance.state.user;
        this.tab = storeInstance.state.tab;
        storeInstance.subscribe(state => {
            this.user = state.user;
            this.tab = state.tab;
            this.requestUpdate();
        });
    }

    render() {
        return html`
        <link rel="stylesheet" type="text/css" href="src/app.css">
        <div class="header block mt-2">
            <div class="flex header--logout justify-end p-2">
                <button class="btn"> LOGOUT </button>
            </div>
            <div class="flex header--coverimage justify-center m-1">
                <button class="btn btn--lg text-secondary flex items-center"> 
                    <i class="icon ion-md-camera text-lg mr-1" aria-hidden="true"></i> Upload Cover Image 
                </button>
            </div>
            <div class="header--info">
                <div class="profile-img">
                    <img class="shadow" src="src/img/profile_image.jpg" />
                </div>

                <div class="profile-info p-2">
                    <div>
                        <div class="profile-info__name text-md mb-1"> ${this.user.name} </div>
                        <div class="text-sm mb-1">  <i class="ion-md-locate mr-1" aria-hidden="true"></i> ${this.user.city} </div>
                        <div class="text-sm mb-1">  <i class="ion-md-call mr-1" aria-hidden="true"></i> ${this.user.phone} </div>
                    </div>
                    <div class="flex">
                       <profile-reviews></profile-reviews>
                    </div>
                </div>
            </div>
             <!-- Tabs -->
             <div class="profile-tabs">
                    <ul>
                        <li class=${ this.tab == 'about' ? 'active' : '' } @click=${() => storeInstance.dispatch('setTab', 'about')}> about </li>
                        <li class=${ this.tab == 'settings' ? 'active' : '' } @click=${() => storeInstance.dispatch('setTab', 'settings')}> settings </li>
                        <li class=${ this.tab == 'media' ? 'active' : '' } @click=${() => storeInstance.dispatch('setTab', 'media')}> media </li>
                        <li> option2 </li>
                        <li> option3 </li>
                    </ul>
                    <div class="text-sm profile-tabs--folowers">
                        <i class="icon ion-md-add-circle text-lg text-gray mr-1" aria-hidden="true"></i>
                        <span class="mr-1 text-black"> 15 </span>
                        <span class="text-secondary"> Followers </span>
                    </div>
             </div>
        </div>`
    }
}
customElements.define('user-profile', Profile);