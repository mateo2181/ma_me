import { LitElement, html, css } from 'lit-element';
import storeInstance from '../../js/main';

class IndexTabs extends LitElement {

    constructor() {
        super();
        this.user = storeInstance.state.user;
        this.tab =  storeInstance.state.tab;
        storeInstance.subscribe(state => {
            // this.user = state.user;
            this.tab = state.tab;
            this.requestUpdate();
        });
    }

    render() {
        return html`
                <link rel="stylesheet" type="text/css" href="src/app.css">
                <div class="bg-white shadow mt-2 mb-2 p-4">
                    <h2 class="text-lg capitalize my-4"> ${ this.tab } </h2>  
                    ${ (this.tab == 'about') ? html`<profile-about class="pt-2"></profile-about>` : html`` }
                    ${ (this.tab == 'settings') ? html`<profile-settings class="pt-2"></profile-settings>` : html`` }
                    ${ (this.tab == 'media') ? html`<profile-media class="pt-2"></profile-media>` : html`` }
                </div>
                `
    }
}
customElements.define('index-tabs', IndexTabs);