import { LitElement, html } from "lit-element";
import storeInstance from '../../js/main';

class About extends LitElement {

    constructor() {
        super();
        this.user = storeInstance.state.user;
        // this.tab =  storeInstance.state.tab;
        storeInstance.subscribe(state => {
            this.user = state.user;
            // this.tab = state.tab;
            this.requestUpdate();
        });
    }

    render() {
        return html`
            <link rel="stylesheet" type="text/css" href="src/app.css">
            <div class="box-fields pt-2">
                <div class="editprofile--desktop">
                    <edit-field .editingMobile="${this.editingMobile}" field="name" .text="${this.user.name}"></edit-field>
                    <edit-field .editingMobile="${this.editingMobile}" field="website" .text="${this.user.website}"></edit-field>
                    <edit-field .editingMobile="${this.editingMobile}" field="phone" .text="${this.user.phone}"></edit-field>
                    <edit-field .editingMobile="${this.editingMobile}" field="city" .text="${this.user.city}"></edit-field>
                </div>
                <div class="editprofile--mobile">
                    <profile-about-mobile></profile-about-mobile>                    
                </div>
            </div>
        `
    }
}

customElements.define('profile-about', About);