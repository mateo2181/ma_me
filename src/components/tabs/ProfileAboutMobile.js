import { LitElement, html } from "lit-element";
import storeInstance from '../../js/main';

class ProfileAboutMobile extends LitElement {

    constructor() {
        super();
        this.editing = false;
        this.user = storeInstance.state.user;
        // this.tab =  storeInstance.state.tab;
        storeInstance.subscribe(state => {
            this.user = state.user;
            this.requestUpdate();
        });
    }

    edit() {
        this.editing = true;
        this.userEditing = this.user;
        this.requestUpdate();
    }

    updateUser() {
        // console.log(value);
        // let obj = {};
        // obj[this.field] = this.textEditing;
        storeInstance.dispatch('setProfile', this.userEditing);
        this.editing = false;
    }

    cancelEdit() {
        this.editing = false;
        this.userEditing = this.user;
        this.requestUpdate();
    }

    render() {
        return html`
            <link rel="stylesheet" type="text/css" href="src/app.css">
            <div class="about-profile-mobile">
                ${(!this.editing) ?  
                html`
                    <div class="flex justify-end">
                        <button class="editmobilebutton" @click=${this.edit}> 
                            <i class="icon ion-md-create mr-1" aria-hidden="true"></i> 
                        </button>
                    </div>
                    <div>
                        <div class="text-md mb-1"> <strong> ${this.user.name} </strong></div>
                        <div class="text-sm mb-1">  <i class="icon ion-md-globe mr-1" aria-hidden="true"></i> ${this.user.website} </div>
                        <div class="text-sm mb-1">  <i class="icon ion-md-call mr-1" aria-hidden="true"></i> ${this.user.phone} </div>
                        <div class="text-sm mb-1">  <i class="icon ion-md-locate mr-1" aria-hidden="true"></i> ${this.user.city} </div>
                    </div>
                `
                :
                html`
                <div>
                    <div class="buttons-editing flex justify-end">
                        <button class="btn btn--link uppercase text-primary text-base" @click=${this.cancelEdit}> Cancel </button>
                        <button class="btn btn--link uppercase text-primary text-base mr-1" @click=${this.updateUser}> Save </button>
                    </div>
                    <div>
                        <div class="group">
                            <input required type="text" @change="${(e) => this.userEditing.name = e.target.value }" .value=${this.userEditing.name} />
                            <span class="highlight"></span><span class="bar"></span>
                            <label class="w-full uppercase text-xs text-gray"> name </label>
                        </div>
                        <div class="group">
                            <input required type="text" @change="${(e) => this.userEditing.website = e.target.value }" .value=${this.userEditing.website} />
                            <span class="highlight"></span><span class="bar"></span>
                            <label class="w-full uppercase text-xs text-gray"> website </label>
                        </div>
                        <div class="group">
                            <input required type="text" @change="${(e) => this.userEditing.phone = e.target.value }" .value=${this.userEditing.phone} />
                            <span class="highlight"></span><span class="bar"></span>
                            <label class="w-full uppercase text-xs text-gray"> phone </label>
                        </div>
                        <div class="group">
                            <input required type="text" @change="${(e) => this.userEditing.city = e.target.value }" .value=${this.userEditing.city} />
                            <span class="highlight"></span><span class="bar"></span>
                            <label class="w-full uppercase text-xs text-gray"> city, state & zip </label>
                        </div>
                    </div>
                </div>
                `
                }
            </div>
        `
    }
}

customElements.define('profile-about-mobile', ProfileAboutMobile);