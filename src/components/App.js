import { LitElement, html } from "lit-element";
import '../app.css';
class MyApp extends LitElement {
    
    render() {
        return html`
        <link rel="stylesheet" type="text/css" href="src/app.css">
        <div class="container">
            <user-profile></user-profile>
            <index-tabs></index-tabs>
        </div>
        `
    }
}

customElements.define('my-app', MyApp);