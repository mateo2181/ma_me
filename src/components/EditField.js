import { LitElement, html, css } from 'lit-element';
import storeInstance from '../js/main';

class EditField extends LitElement {
    static get properties() {
        return {
            text: {
                type: String,
                reflect: true
            },
            field: {
                type: String,
                reflect: true
            }
        }
    }

    constructor() {
        super();
        this.editing = false;
        this.textEditing = this.text;
    }

    updateField() {
        // console.log(value);
        let obj = {};
        obj[this.field] = this.textEditing;
        storeInstance.dispatch('setProfile', obj);
        this.editing = false;
    }

    edit() {
        this.editing = true;
        this.textEditing = this.text;
        this.requestUpdate();
    }

    cancelEdit() {
        this.editing = false;
        this.textEditing = this.text;
        this.requestUpdate();
    }

    render() {
        return html`
            <link rel="stylesheet" type="text/css" href="src/app.css">
            <div class="relative profile-field">
                <div class="flex">
                    <div class="text-sm field-label"> ${ this.text} </div>
                    <button class="field-editbutton" @click=${this.edit}> 
                        <i class="icon ion-md-create mr-1" aria-hidden="true"></i>
                    </button>
                </div>
                ${ (this.editing) ? html`
                    <div class="tooltip-edit">
                        <div class="group">
                            <input required @change="${(e) => this.textEditing = e.target.value }" .value=${this.text} type="text"/>
                            <span class="highlight"></span><span class="bar"></span>
                            <label class="w-full uppercase text-xs text-gray"> ${this.field} </label>
                        </div>
                        <!-- <label class="w-full uppercase text-xs text-gray"> ${this.field} </label> -->
                        <!-- <input class="input" type="text" @change="${(e) => this.textEditing = e.target.value }" .value=${this.text} /> -->
                        <div class="flex mt-1">
                            <button class="btn uppercase btn--primary mr-1" @click=${this.updateField}> Save </button>
                            <button class="btn uppercase btn--primary--outline" @click=${this.cancelEdit}> Cancel </button>
                        </div>
                    </div>`
                    :
                    html``
                }
            </div>
        `;
    }
}

customElements.define('edit-field', EditField);