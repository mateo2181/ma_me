
import "./components/App.js";
import "./components/Profile.js";
import "./components/tabs/Index.js";
import "./components/tabs/About.js";
import "./components/tabs/Settings.js";
import "./components/tabs/Media.js";
import "./components/EditField.js";
import "./components/Reviews.js";
import "./components/tabs/ProfileAboutMobile";