const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = ({ mode }) => {
  return {
    module: {
      rules: [
        {
          test: /\.js$/,
          include: [ /node_modules(?:\/|\\)lit-element|lit-html/],
          use: ['babel-loader']
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            // Creates `style` nodes from JS strings
            'style-loader',
            // Translates CSS into CommonJS
            'css-loader',
            // Compiles Sass to CSS
            'sass-loader',
          ],
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html'
      }),
      new CopyWebpackPlugin([
        {
          context: 'node_modules/@webcomponents/webcomponentsjs',
          from: '**/*.js',
          to: 'webcomponents'
        },
        {
          context: 'src/img',
          from: '*.jpg',
          to: 'src/img'
        },
        {
          context: 'src',
          from: 'app.css',
          to: 'src'
        }
      ])
    ],
    devtool: mode === 'development' ? 'source-map' : 'none'
  };
};